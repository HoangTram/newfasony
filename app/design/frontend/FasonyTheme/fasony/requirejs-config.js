var config = {
    map: {
        '*': {
            flexSlider: 'Magento_Cms/js/jquery.flexslider',
            slickSlider: 'Magento_Cms/js/slick',
        }
    }
};