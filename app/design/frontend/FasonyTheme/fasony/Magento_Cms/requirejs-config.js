var config = {
    "map": {
        "*": {
            "owlCarousel": 'owl.carousel/owl.carousel',
            "megamenu": 'megamenu-js-master/megamenu'
        }
    },
    waitSeconds: 40,
    "shim": {
        "owlCarousel": {
            deps: ["jquery"]
        },
        "megamenu": {
            deps: ["jquery"]
        }
    }
};