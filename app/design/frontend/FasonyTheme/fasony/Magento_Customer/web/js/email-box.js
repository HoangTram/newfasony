// requirejs(['jquery'], function(jQuery) {
//     jQuery(document).ready(function () {
//         "use strict";
//         jQuery(document).ready(function($) {
//             jQuery('.signup__close').click(function(){
//                 jQuery('.signup--modal').hide();
//             });
//         });
//
//         jQuery(document).ready(function($) {
//             jQuery('.switcher-trigger').click(function(){
//                 jQuery('.signup--modal').show();
//             });
//         });
//     });
// });

define([
        'jquery',
        'jquery/ui'
], function($) {
    "use strict";

    var cookieManager = {
        setCookie: function (cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toGMTString();
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        },

        getCookie: function (cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        },

        checkCookie: function () {
            var user = getCookie("username");
            if (user != "") {
                jQuery(document).ready(function ($) {
                    jQuery('.signup--modal').css("display", "none");
                });
            } else {
                user = prompt("Please enter your name:", "");
                jQuery(document).ready(function ($) {
                    jQuery('.signup--modal').css("display", "block");
                });
                if (user != "" && user != null) {
                    setCookie("username", user, 30);
                }
            }
        }
    }

    $(".signup__close").click(function () {
        cookieManager.setCookie('username', 'value', 1000);
        jQuery('.signup--modal').css("display", "none");
    });

    return cookieManager;
});