define(
    [
        'jquery',
        'ko',
        './customer/address'
    ],
    function($, ko, address) {
        "use strict";
        var isLoggedIn = ko.observable(window.isCustomerLoggedIn);
        var_dump(isLoggedIn);
        return {
            getAddressItems: function() {
                var items = [];
                if (isLoggedIn()) {
                    var customerData = window.customerData;
                    if (Object.keys(customerData).length) {
                        $.each(customerData.addresses, function (key, item) {
                            items.push(new address(item));
                        });
                    }
                }
                return items;
            }
        }
    }
);