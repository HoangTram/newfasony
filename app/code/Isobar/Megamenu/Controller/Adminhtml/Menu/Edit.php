<?php
namespace Isobar\Megamenu\Controller\Adminhtml\Menu;

class Edit extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Isobar\Megamenu\Api\MegamenuRepositoryInterface
     */
    protected $megaMenuReposity;

    /**
     * @var \Isobar\Megamenu\Api\Data\MegamenuInterfaceFactory
     */
    protected $megaMenuFactory;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Isobar\Megamenu\Api\MegamenuRepositoryInterface $megaMenuReposity,
        \Isobar\Megamenu\Api\Data\MegamenuInterfaceFactory $megaMenuFactory
    ) {
        $this->megaMenuReposity = $megaMenuReposity;
        $this->megaMenuFactory = $megaMenuFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * Edit Isobar Mega Menu
     *
     * @return \Magento\Framework\Controller\ResultInterface
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $model = $this->megaMenuFactory->create();

        if ($id) {
            $model = $this->megaMenuReposity->get($id);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This magamenu no longer exists.'));
                /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Isobar_Megamenu::megamenu')->addBreadcrumb(__('Mega Menu'), __('Mega Menu'))
            ->addBreadcrumb(
            $id ? __('Edit Block') : __('New Mega Menu'),
            $id ? __('Edit Block') : __('New Mega Menu')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Mega Menu'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? $model->getTitle() : __('New Mega Menu'));
        return $resultPage;
    }
}
