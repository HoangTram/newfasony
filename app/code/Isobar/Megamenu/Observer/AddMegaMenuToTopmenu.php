<?php
/**
 * Copyright © 2016 Skyfronts. All rights reserved.
 * See LICENSE.txt for license details.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Skyfronts_Smartmenu to newer
 * versions in the future.
 *
 * @category    Skyfronts
 * @package     Skyfronts_Smartmenu
 * @author      Skyfronts team
 * @copyright   Copyright (c) Skyfronts team
 */

namespace Isobar\Megamenu\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Data\Tree\Node;
use Magento\Framework\Event\ObserverInterface;
use Magento\Catalog\Api\CategoryRepositoryInterface;

class AddMegaMenuToTopmenu implements ObserverInterface
{
    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */

    protected $searchCriteriaBuilder;

    /**
     * @var \Isobar\Megamenu\Api\MegamenuRepositoryInterface
     */
    protected $megaMenuRepository;

    /**
     * @var \Magento\Framework\Api\SortOrder
     */
    protected $sortOrder;

    /**
     * @var \Magento\Framework\Api\FilterBuilder
     */
    protected $filterBuilder;

    /**
     * @var \Magento\Framework\Api\Search\FilterGroupBuilder
     */

    /**
     * @var \Magento\Framework\Api\Search\FilterGroupBuilder
     */
    protected $filterGroupBuilder;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlInterface;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var CategoryRepositoryInterface
     */
    protected $categoryRepository;

    /**
     * AddMegaMenuToTopmenu constructor.
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Isobar\Megamenu\Api\MegamenuRepositoryInterface $megaMenuRepository
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\Framework\Api\SortOrder $sortOrder
     * @param \Magento\Framework\Api\FilterBuilder $filterBuilder
     * @param \Magento\Framework\Api\Search\FilterGroupBuilder $filterGroupBuilder
     * @param \Magento\Framework\UrlInterface $urlInterface
     * @param \Magento\Framework\Registry $registry
     * @param CategoryRepositoryInterface $categoryRepository
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Isobar\Megamenu\Api\MegamenuRepositoryInterface $megaMenuRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\Api\SortOrder $sortOrder,
        \Magento\Framework\Api\FilterBuilder $filterBuilder,
        \Magento\Framework\Api\Search\FilterGroupBuilder $filterGroupBuilder,
        \Magento\Framework\UrlInterface $urlInterface,
        \Magento\Framework\Registry $registry,
        CategoryRepositoryInterface $categoryRepository
    ) {
        $this->urlInterface = $urlInterface;
        $this->_storeManager = $storeManager;
        $this->megaMenuRepository = $megaMenuRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->sortOrder = $sortOrder;
        $this->filterBuilder = $filterBuilder;
        $this->filterGroupBuilder = $filterGroupBuilder;
        $this->registry = $registry;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Get megamenu items
     * @return \Isobar\Megamenu\Api\Data\MegamenuInterface[]
     */
    public function getMenuItems()
    {
        $filterGroups = [];
        $searchCriteria = $this->searchCriteriaBuilder
            ->setFilterGroups($filterGroups)
            ->create();
        $sortOrder = $this->sortOrder->setField('sort')->setDirection(\Magento\Framework\Api\SortOrder::SORT_ASC);
        $searchCriteria->setSortOrders([$sortOrder]);
        $result = $this->megaMenuRepository->getList($searchCriteria);
        $items = $result->getItems();
        return $items;
    }

    /**
     * Execute
     * @param EventObserver $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $storeMenuItems = $this->getMenuItems();
        $this->_addMegamenuToMenu($storeMenuItems, $observer->getMenu());
        return $this;
    }

    /**
     * Add megamenu to top menu
     * @param $storeMenuItems
     * @param $parentCategoryNode
     */
    protected function _addMegamenuToMenu($storeMenuItems, $parentCategoryNode)
    {
        $parentNodes = $parentCategoryNode->getChildren();
        if (count($storeMenuItems) > 0) {
            foreach ($parentNodes as $value) {
                $parentCategoryNode->removeChild($value);
            }
        }
        return;
        $tree = $parentCategoryNode->getTree();
        $treeData = $this->_getMenuItemTreeData($storeMenuItems);
        $this->_buildMegamenu(0, $treeData, $tree, $parentCategoryNode);
    }

    /**
     * Get menu item to tree data
     * @param $items
     * @return array
     */
    protected function _getMenuItemTreeData($items)
    {
        $menuItemData = array(
            'items' => array(),
            'parents' => array()
        );
        foreach ($items as $menuItem) {
            $menuItemData ['items'] [$menuItem->getId()] = $menuItem;
            $menuItemData ['parents'] [$menuItem->getParentId()] [] = $menuItem->getId();
        }
        return $menuItemData;
    }

    /**
     * Build megamenu
     * @param $parentId
     * @param $treeData
     * @param $tree
     * @param $menu
     */
    protected function _buildMegamenu($parentId, $treeData, $tree, $menu)
    {
        if (isset($treeData['parents'][$parentId])) {
            $index = 0;
            foreach ($treeData['parents'][$parentId] as $itemId) {
                $nodeId = 'category-node-'.$treeData['items'][$itemId]->getId();
                $data = [
                    'name'      => $treeData['items'][$itemId]->getTitle(),
                    'id'        => $nodeId,
                    'url'       => $treeData['items'][$itemId]->getLink(),
                    'is_active' => $this->_checkIsActive($treeData['items'][$itemId]->getLink())
                ];
                $node = new Node($data, 'id', $tree, $menu);
                $menu->addChild($node);
                $this->_buildMegamenu($itemId, $treeData, $tree, $node);
            }
        }
    }

    /**
     * Check category is active
     * @param $link
     * @return bool
     */
    protected function _checkIsActive($link)
    {
        $category = $this->registry->registry('current_category');
        if ($category) {
            $catUrl = $this->_getCategoryUrl($category->getId());
            return $link === $catUrl;
        }
        return false;
    }

    /**
     * @param $categoryId
     * @return mixed
     */
    protected function _getCategoryUrl($categoryId)
    {
        $category = $this->categoryRepository->get($categoryId, $this->_storeManager->getStore()->getId());
        return $category->getUrl();
    }
}
