<?php
namespace Isobar\Megamenu\Block\Adminhtml\Megamenu\Edit;

use Magento\Backend\Block\Widget\Context;
use Isobar\Megamenu\Api\MegamenuRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class GenericButton
 */
class GenericButton
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var MegamenuRepositoryInterface
     */
    protected $megaMenuRepository;

    /**
     * @param Context $context
     * @param MegamenuRepositoryInterface $megaMenuRepository
     */
    public function __construct(
        Context $context,
        MegamenuRepositoryInterface $megaMenuRepository
    ) {
        $this->context = $context;
        $this->megaMenuRepository = $megaMenuRepository;
    }

    /**
     * Return Banner ID
     *
     * @return int|null
     */
    public function getId()
    {
        try {
            return $this->megaMenuRepository->get(
                $this->context->getRequest()->getParam('id')
            )->getId();
        } catch (NoSuchEntityException $e) {
        }
        return null;
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
