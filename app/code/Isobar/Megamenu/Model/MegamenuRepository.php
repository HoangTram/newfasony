<?php

namespace Isobar\Megamenu\Model;

class MegamenuRepository implements \Isobar\Megamenu\Api\MegamenuRepositoryInterface {
    /**
     * @var \Isobar\Megamenu\Model\MegamenuFactory
     */
    protected $modelFactory;

    /**
     * @var \Isobar\Megamenu\Model\ResourceModel\Megamenu
     */
    protected $resourceModel;

    /**
     * @var \Isobar\Megamenu\Model\ResourceModel\Megamenu\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var \Isobar\Megamenu\Api\Data\MegamenuSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var \Isobar\Megamenu\Api\FilterBuilder
     */
    protected $filterBuilder;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * MegamenuRepository constructor.
     * @param MegamenuFactory $modelFactory
     * @param ResourceModel\Megamenu $resourceModel
     * @param ResourceModel\Megamenu\CollectionFactory $collectionFactory
     * @param \Isobar\Megamenu\Api\Data\MegamenuSearchResultsInterfaceFactory $searchResultsFactory
     * @param \Magento\Framework\Api\FilterBuilder $filterBuilder
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        \Isobar\Megamenu\Model\MegamenuFactory $modelFactory,
        \Isobar\Megamenu\Model\ResourceModel\Megamenu $resourceModel,
        \Isobar\Megamenu\Model\ResourceModel\Megamenu\CollectionFactory $collectionFactory,
        \Isobar\Megamenu\Api\Data\MegamenuSearchResultsInterfaceFactory $searchResultsFactory,
        \Magento\Framework\Api\FilterBuilder $filterBuilder,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->modelFactory = $modelFactory;
        $this->resourceModel = $resourceModel;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->filterBuilder = $filterBuilder;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * {@inheritdoc}
     */
    public function save(\Isobar\Megamenu\Api\Data\MegamenuInterface $model) {
        try {
            $this->resourceModel->save($model);
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(__('Unable to save mega menu'));
        }
        return $model;
    }

    /**
     * {@inheritdoc}
     */
    public function get($modelId) {
        $model = $this->modelFactory->create();
        $this->resourceModel->load($model, $modelId);
        if(!$model->getId()) {
            throw new \Magento\Framework\Exception\NoSuchEntityException(__('Requested mega menu doesn\'t exist'));
        }
        return $model;
    }

    /**
     * {@inheritdoc}
     */
    public function getById($id)
    {
        $megaMenu = $this->modelFactory->create();
        $this->resourceModel->load($megaMenu, $id);
        if (!$megaMenu->getId()) {
            throw new NoSuchEntityException(__('Banner with id "%1" does not exist.', $id));
        }
        return $megaMenu;
    }
    /**
     * {@inheritdoc}
     */
    public function delete(\Isobar\Megamenu\Api\Data\MegamenuInterface $model) {
        $id = $model->getId();
        try {
            $this->resourceModel->delete($model);
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\StateException(__('Unable to remove mega menu %1', $id));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($modelId) {
        $model = $this->get($modelId);
        return $this->delete($model);
    }

    /**
     * {@inheritdoc}
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria) {
        /** @var \Isobar\Megamenu\Model\ResourceModel\Megamenu\Collection $collection */
        $collection = $this->collectionFactory->create();

        //Add filters from root filter group to the collection
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->addFilterGroupToCollection($group, $collection);
        }

        /** @var \Magento\Framework\Api\SortOrder $sortOrder */
        foreach ((array)$searchCriteria->getSortOrders() as $sortOrder) {
            $field = $sortOrder->getField();
            $collection->addOrder(
                $field,
                ($sortOrder->getDirection() == \Magento\Framework\Api\SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
            );
        }

        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());
        $collection->load();

        /** @var \Isobar\Megamenu\Api\Data\MegamenuSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * Helper function that adds a FilterGroup to the collection.
     *
     * @param \Magento\Framework\Api\Search\FilterGroup $filterGroup
     * @param \Isobar\Megamenu\Model\ResourceModel\Megamenu\Collection $collection
     * @return void
     */
    private function addFilterGroupToCollection(
        \Magento\Framework\Api\Search\FilterGroup $filterGroup,
        \Isobar\Megamenu\Model\ResourceModel\Megamenu\Collection $collection)
    {
        $fields = [];
        $conditions = [];

        foreach($filterGroup->getFilters() as $filter){
            $field = $filter->getField();
            $condition = $filter->getConditionType() ?: 'eq';
            $value = $filter->getValue();

            $fields[] = $field;
            $conditions[] = [ $condition => $value ];
        }

        if ($fields) {
            $collection->addFieldToFilter($fields, $conditions);
        }
    }
}