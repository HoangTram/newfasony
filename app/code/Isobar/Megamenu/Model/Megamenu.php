<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 12/06/2017
 * Time: 15:04
 */

namespace Isobar\Megamenu\Model;


class Megamenu extends \Magento\Framework\Model\AbstractModel implements \Isobar\Megamenu\Api\Data\MegamenuInterface
{
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    /**
     * construct
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Isobar\Megamenu\Model\ResourceModel\Megamenu');
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->_getData('id');
    }

    /**
     * {@inheritdoc}
     */
    public function setId($id)
    {
        return $this->setData('id', $id);
    }

    /**
     * {@inheritdoc}
     */
    public function getTitle()
    {
        return $this->_getData(self::TITLE);
    }

    /**
     * {@inheritdoc}
     */
    public function setTitle($title)
    {
        return $this->setData('title', $title);
    }

    /**
     * {@inheritdoc}
     */
    public function getLink()
    {
        return $this->_getData(self::LINK);
    }

    /**
     * {@inheritdoc}
     */
    public function setLink($link)
    {
        return $this->setData('link', $link);
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return $this->_getData(self::DESCRIPTION);
    }

    /**
     * {@inheritdoc}
     */
    public function setDescription($description)
    {
        return $this->setData('description', $description);
    }

    /**
     * {@inheritdoc}
     */
    public function getExtendHtml()
    {
        return $this->_getData(self::EXTEND_HTML);
    }

    /**
     * {@inheritdoc}
     */
    public function setExtendHtml($extendHtml)
    {
        return $this->setData('extend_html', $extendHtml);
    }

    /**
     * {@inheritdoc}
     */
    public function getSort()
    {
        return $this->_getData(self::SORT);
    }

    /**
     * {@inheritdoc}
     */
    public function setSort($sort)
    {
        return $this->setData('sort', $sort);
    }

    /**
     * {@inheritdoc}
     */
    public function getStatus()
    {
        return $this->_getData(self::STATUS);
    }

    /**
     * {@inheritdoc}
     */
    public function setStatus($status)
    {
        return $this->setData('status', $status);
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedAt() {
        return $this->_getData(self::CREATED_AT);
    }

    /**
     * {@inheritdoc}
     */
    public function setCreatedAt($timeStamp) {
        return $this->setData(self::CREATED_AT, $timeStamp);
    }
}