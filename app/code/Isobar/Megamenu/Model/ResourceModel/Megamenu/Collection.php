<?php

namespace Isobar\Megamenu\Model\ResourceModel\Megamenu;


class Collection extends \Isobar\Megamenu\Model\ResourceModel\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';
    
    /**
     * _contruct
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Isobar\Megamenu\Model\Megamenu', 'Isobar\Megamenu\Model\ResourceModel\Megamenu');
    }
}