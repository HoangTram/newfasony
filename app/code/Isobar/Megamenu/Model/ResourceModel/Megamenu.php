<?php

namespace Isobar\Megamenu\Model\ResourceModel;


class Megamenu extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * construct
     * @return void
     */
    protected function _construct()
    {
        $this->_init('isobar_mega_menu', 'id');
    }
}